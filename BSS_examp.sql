-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Máy chủ: localhost:3306
-- Thời gian đã tạo: Th3 24, 2022 lúc 12:15 AM
-- Phiên bản máy phục vụ: 8.0.28-0ubuntu0.20.04.3
-- Phiên bản PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `BSS_examp`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `ActionLogs`
--

CREATE TABLE `ActionLogs` (
  `id_logs` int NOT NULL,
  `id_device` int NOT NULL,
  `action` varchar(25) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Đang đổ dữ liệu cho bảng `ActionLogs`
--

INSERT INTO `ActionLogs` (`id_logs`, `id_device`, `action`, `date`) VALUES
(7, 1, 'Turn on', '2022-03-01'),
(8, 2, 'Turn off', '2022-03-01'),
(10, 1, 'Turn on', '2022-03-25'),
(11, 3, 'Turn off', '2022-03-23'),
(12, 4, 'Turn on', '2022-03-09'),
(13, 2, 'Turn off', '2022-03-16'),
(14, 4, 'Turn off', '2022-03-31'),
(15, 3, 'Turn on', '2022-03-31'),
(16, 4, 'Turn on', '2022-03-11');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `admin`
--

CREATE TABLE `admin` (
  `id` int NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL,
  `text` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Đang đổ dữ liệu cho bảng `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`, `text`) VALUES
(1, 'dong', '12345678', 'IMG-623b54dd8137b3.25931858.png');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `Devices`
--

CREATE TABLE `Devices` (
  `id_device` int NOT NULL,
  `name_device` varchar(25) NOT NULL,
  `mac_address` varchar(50) NOT NULL,
  `ip_device` varchar(50) NOT NULL,
  `create_date` date NOT NULL,
  `power_device` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Đang đổ dữ liệu cho bảng `Devices`
--

INSERT INTO `Devices` (`id_device`, `name_device`, `mac_address`, `ip_device`, `create_date`, `power_device`) VALUES
(1, 'IV', '00:1B:44:11:3A:B7', '127.0.0.2', '2022-03-14', 50),
(2, 'Washer', '00:1B:44:11:3A:B7', '127.0.0.2', '2022-03-14', 60),
(3, 'Refrigerator', '00:1B:44:11:3A:B7', '127.0.0.2', '2022-03-14', 90),
(4, 'Selling Fan', '00:1B:44:11:3A:B7', '127.0.0.2', '2022-03-14', 56);

--
-- Bẫy `Devices`
--
DELIMITER $$
CREATE TRIGGER `delete_device` BEFORE DELETE ON `Devices` FOR EACH ROW BEGIN
	DELETE FROM ActionLogs WHERE id_device=OLD.id_device;
END
$$
DELIMITER ;

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `ActionLogs`
--
ALTER TABLE `ActionLogs`
  ADD PRIMARY KEY (`id_logs`),
  ADD KEY `id_device` (`id_device`);

--
-- Chỉ mục cho bảng `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `Devices`
--
ALTER TABLE `Devices`
  ADD PRIMARY KEY (`id_device`),
  ADD UNIQUE KEY `name_device` (`name_device`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `ActionLogs`
--
ALTER TABLE `ActionLogs`
  MODIFY `id_logs` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT cho bảng `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `Devices`
--
ALTER TABLE `Devices`
  MODIFY `id_device` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `ActionLogs`
--
ALTER TABLE `ActionLogs`
  ADD CONSTRAINT `ActionLogs_ibfk_1` FOREIGN KEY (`id_device`) REFERENCES `Devices` (`id_device`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
