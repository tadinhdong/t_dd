<?php
require_once('controllers/base_controller.php');
require_once('models/Devices.php');
class LogsController extends BaseController
{

    function __construct()
    {
        $this->folder = 'Logs';
    }

    function index(){
        $das=$this -> getData();
        $title = array();
        $index = array();
        foreach ($das as $data) {
            $title[]=$data -> getNameDevice();
            $index[]=$data -> getPowerDevice();
        }

        $data = array('datas' => $das,'titles' => $title, 'indexs' => $index);

        $this->render('index',$data);
    }

    function addItem(){
        if(isset($_GET['device'])){
            $data=$_GET['device'];
        }
        $id_device =Device::findId($data["devices"]);
        if($id_device!=null){
            echo 2;
        }else{
            $req = Device::insert($data["devices"],$data["mac_address"],$data["ip"],$data["createdDate"],$data["powerConsumption"]);
            if($req ==0){
                $this ->index();
            }else{
                echo 1;
            }
        }
    }


    function getData(){
        $das=Device::getAll();
        return $das;
    }


    function delete(){
        if(isset($_POST['id_item'])){
            $id_item=$_POST['id_item'];
            if(Device::delete($id_item)==0){
                $this->index();
            }else{
                echo 1;
            };
        }else{
            echo 2;
        }
    }

}