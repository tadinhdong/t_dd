<?php
require_once('controllers/base_controller.php');
require_once('models/ActionLogs.php');
class DasController extends BaseController
{

    function __construct()
    {
        $this->folder = 'Dasboard';
    }

    function index(){
        $data = $this -> getData();
        $this->render('index',$data);
    }

    function delete(){
        if(isset($_POST['id_item'])){
            $id_item=$_POST['id_item'];
            if(ActionLogs::delete($id_item)==0){
                $this->index();
            }else{
                echo 1;
            };
        }else{
            echo 2;
        }
    }

    function error(){
        echo "error";
    }

    function getData(){
        $page=1;
        $row_in_page=5;
        if(isset($_GET['page'])){
            $page=$_GET['page'];
        }
        if(isset($_GET['row_in_page'])){
            $row_in_page=$_GET['row_in_page'];
        }
//        echo $row_in_page;
        $sum_row=ActionLogs::countData();
        $number_page=ceil($sum_row/$row_in_page);
        $from=($page-1)*$row_in_page;
        $das=ActionLogs::getLimit($from,$row_in_page);
        $data = array('datas' => $das,'page' => $page,'number_page' => $number_page,'row_in_page' =>$row_in_page);
//        var_dump($data);
        return $data;
    }

}