<?php
require_once('models/Admin.php');
class LoginController {

    function __construct()
    {

    }

    public function index(){
        if(isset($_SESSION["admin"])){
        header("Location:index.php");
        }else{
            $check = false;
            if(isset($_COOKIE["Admin"]) ){
                $admin = $_COOKIE["Admin"];
                $check = true;
            }
        }
        require_once('views/Login/login.php');
    }

    public function login(){
        if(isset($_POST["username"]) && isset($_POST["password"])){
            $user = $_POST["username"];
            $pass = $_POST["password"];
            $req =Admin::login($user,$pass);
            if($req == null){
                echo 1;
            }else{
                $admin = new Admin($req['id'],$req['username'],$req['password'],$req['text']);
                //lưu session
                $_SESSION["admin"] = $admin->getId();
                //lưu cookie
                if(isset($_POST["checked"])&&$_POST["checked"]==0){
                    setcookie("Admin",$admin,time() + 84600*2);
                }else{
                    setcookie("Admin",$admin,time() - 84600);
                }
               echo 0;
            }
        }else{
            echo 1;
        }
    }

    public function logout(){
        unset($_SESSION["admin"]);
        header("Location:index.php?controller=login");
    }
}