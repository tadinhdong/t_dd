<?php
require_once('controllers/base_controller.php');
require_once('models/Admin.php');
class AdminController extends BaseController
{
    function __construct()
    {
        $this->folder = 'Admin';
    }

    function index(){
        if(isset($_SESSION["admin"])){
            $id =$_SESSION["admin"] ;
        }
        $req=Admin::getAdmin($id);
        $data = array('admin' => $req);
        $this->render('index',$data);
    }

    function upload(){
        if(isset($_SESSION["admin"])){
            $id =$_SESSION["admin"] ;
        }
        if (isset($_FILES['my_image'])) {
            $img_name = $_FILES['my_image']['name'];
            $img_size = $_FILES['my_image']['size'];
            $tmp_name = $_FILES['my_image']['tmp_name'];
            $error    = $_FILES['my_image']['error'];
            if ($error === 0) {
                if ($img_size > 1000000) {
                    $em = "Xin lỗi, tệp của bạn quá lớn.";

                    # response array
                    $error = array('error' => 1, 'em'=> $em);

                    echo json_encode($error);
                    exit();
                }else {
                    $img_ex = pathinfo($img_name, PATHINFO_EXTENSION);
                    //lấy đuôi mở rộng
                    $img_ex_lc = strtolower($img_ex);

                    $allowed_exs = array("jpg", "jpeg", "png");
                    //kiểm tra phần mở rộng
                    if (in_array($img_ex_lc, $allowed_exs)) {
                    //tạo tên mới cho ảnh
                        $new_img_name = uniqid("IMG-", true).'.'.$img_ex_lc;
                    // upload vào thư mục
                        $img_upload_path = "views/Public/uploads/".$new_img_name;

                        move_uploaded_file($tmp_name, $img_upload_path);

                        //update
                        $req= Admin::upload($id,$new_img_name);
                        //nếu trả về 0 là update hoàn tất
                        if($req==0){
                            $error = array('error' => 0, 'src'=> $new_img_name);
                        }else{
                            //quá trình up date xảy ra sai xót
                            $em = "Không thể đưa dữ liệu lên server";
                            $error = array('error' => 1, 'em'=> $em);
                        }
                        echo json_encode($error);
                        exit();

                    }else {
                        $em = "Bạn không thể tải lên các tệp thuộc loại này!";

                        $error = array('error' => 1, 'em'=> $em);
                        echo json_encode($error);
                        exit();
                    }
                }
            }else {
                $em = "Đã có lỗi xảy ra!";
                $error = array('error' => 1, 'em'=> $em);
                echo json_encode($error);
                exit();
            }
        }
    }
}