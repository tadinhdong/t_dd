<?php
require_once('models/base_sql.php');
require_once('models/Devices.php');
class ActionLogs
{
    protected $id_log;

    protected $Device;

    protected $action;

    protected $date;

    /**
     * @param $id_log
     * @param $Device
     * @param $action
     * @param $date
     */
    public function __construct($id_log, $Device, $action, $date)
    {
        $this->id_log = $id_log;
        $this->Device = $Device;
        $this->action = $action;
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getIdLog()
    {
        return $this->id_log;
    }

    /**
     * @param mixed $id_log
     */
    public function setIdLog($id_log)
    {
        $this->id_log = $id_log;
    }

    /**
     * @return mixed
     */
    public function getDevice()
    {
        return $this->Device;
    }

    /**
     * @param mixed $Device
     */
    public function setDevice($Device)
    {
        $this->Device = $Device;
    }

    /**
     * @return mixed
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param mixed $action
     */
    public function setAction($action)
    {
        $this->action = $action;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    //lấy tất cả bản ghi
    static function getAll(){
        $list = [];
        $sql = 'SELECT * FROM `ActionLogs`';
        $req =BaseQuery::getData($sql);
        foreach ($req as $item) {
            $id_device =$item['id_device'];
            $device = "SELECT * FROM `Devices` WHERE `id_device`='$id_device'";
            foreach (BaseQuery::getData($device) as $item_device){
                $name_device = $item_device['name_device'];
            }
            $list[] = new ActionLogs($item['id_logs'], $name_device,$item['action'],$item['date']);
        }
        return $list;
    }

    // lấy bản ghi theo limit để phân trang
    static function getLimit($start,$from){
        $sql = "SELECT * FROM `ActionLogs` LIMIT {$start},{$from}";
        $req =BaseQuery::getData($sql);
        foreach ($req as $item) {
            $id_device =$item['id_device'];;
            $device = "SELECT * FROM `Devices` WHERE `id_device`='$id_device'";
            foreach (BaseQuery::getData($device) as $item_device){
                $name_device = $item_device['name_device'];
            }
            $list[] = new ActionLogs($item['id_logs'],$name_device,$item['action'],$item['date']);
        }
        return $list;
    }

    //đếm số bản ghi
    static function countData(){
        $sql = 'SELECT * FROM `ActionLogs`';
        return BaseQuery::getData($sql)->num_rows;
    }


    // xóa bản ghi
    static function delete($id_item){
        $sql = "DELETE FROM `ActionLogs` WHERE `id_logs`={$id_item}";
        return BaseQuery::DE_UP($sql);
    }
}