<?php
require_once('models/base_sql.php');
class Device  {
    protected $id_device;
    protected $name_device;
    protected $mac_address;
    protected $ip_device;
    protected $create_date;
    protected $power_device;

    /**
     * @param $id_device
     * @param $name_device
     * @param $mac_address
     * @param $ip_device
     * @param $create_date
     * @param $power_device
     */
    public function __construct($id_device, $name_device, $mac_address, $ip_device, $create_date, $power_device)
    {
        $this->id_device = $id_device;
        $this->name_device = $name_device;
        $this->mac_address = $mac_address;
        $this->ip_device = $ip_device;
        $this->create_date = $create_date;
        $this->power_device = $power_device;
    }


    /**
     * @return mixed
     */
    public function getIdDevice()
    {
        return $this->id_device;
    }

    /**
     * @param mixed $id_device
     */
    public function setIdDevice($id_device)
    {
        $this->id_device = $id_device;
    }

    /**
     * @return mixed
     */
    public function getNameDevice()
    {
        return $this->name_device;
    }

    /**
     * @param mixed $name_device
     */
    public function setNameDevice($name_device)
    {
        $this->name_device = $name_device;
    }

    /**
     * @return mixed
     */
    public function getMacAddress()
    {
        return $this->mac_address;
    }

    /**
     * @param mixed $mac_address
     */
    public function setMacAddress($mac_address)
    {
        $this->mac_address = $mac_address;
    }

    /**
     * @return mixed
     */
    public function getIpDevice()
    {
        return $this->ip_device;
    }

    /**
     * @param mixed $ip_device
     */
    public function setIpDevice($ip_device)
    {
        $this->ip_device = $ip_device;
    }

    /**
     * @return mixed
     */
    public function getCreateDate()
    {
        return $this->create_date;
    }

    /**
     * @param mixed $create_date
     */
    public function setCreateDate($create_date)
    {
        $this->create_date = $create_date;
    }

    /**
     * @return mixed
     */
    public function getPowerDevice()
    {
        return $this->power_device;
    }

    /**
     * @param mixed $power_device
     */
    public function setPowerDevice($power_device)
    {
        $this->power_device = $power_device;
    }

    //lấy tất cả bản ghi
    static function getAll(){
        $list = [];
        $sql = 'SELECT * FROM `Devices`';
        $req =BaseQuery::getData($sql);
        foreach ($req as $item) {
            $list[] = new Device($item['id_device'],$item['name_device'],$item['mac_address'],$item['ip_device'],$item['create_date'],$item['power_device']);
        }
        return $list;
    }


    static function insert($name_device,$mac_address,$ip_device,$create_date,$power_device){
        $sql = "INSERT INTO `Devices`(`name_device`, `mac_address`, `ip_device`, `create_date`, `power_device`) VALUES ('{$name_device}','{$mac_address}','{$ip_device}','{$create_date}','{$power_device}')";
        $req = BaseQuery::DE_UP($sql);
        return $req;
//        return $sql;
    }

    static function findId($name_device){
        $list = null;
        $sql = "SELECT * FROM `Devices` WHERE `Devices`.`name_device`='{$name_device}' ";
        $req =BaseQuery::getData($sql);
        foreach ($req as $item) {
            $list = new Device($item['id_device'],$item['name_device'],$item['mac_address'],$item['ip_device'],$item['create_date'],$item['power_device']);
        }
        return $list;
    }

    static function delete($id_item){
        $sql = "DELETE FROM `Devices` WHERE `id_device`={$id_item}";
        return BaseQuery::DE_UP($sql);
    }

}