<?php
require_once('models/base_sql.php');
class Admin
{
    public $id;
    public $username;
    public $password;
    public $text;
    /**
     * @param $id
     * @param $username
     * @param $password
     * @param $text
     */
    public function __construct($id, $username, $password, $text)
    {
        $this->id = $id;
        $this->username = $username;
        $this->password = $password;
        $this->text = $text;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param mixed $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    static function login($name,$pass){
        $sql ="SELECT * FROM `admin` WHERE username = '{$name}' AND password ='{$pass}'";
        $req =BaseQuery::getData($sql);
        if($req ==null){
            return null;
        }else{
            return $req->fetch_assoc();
        }
    }

    static function upload($id,$name_img){
        $sql = "UPDATE `admin` SET `text`='{$name_img}' WHERE `id`={$id}";
        return BaseQuery::DE_UP($sql);
    }

    static function getAdmin($id){
        $sql = "SELECT * FROM `admin` WHERE `id`={$id}";
        $req=BaseQuery::getData($sql);
        $admin = null;
        foreach ($req as $item) {
            $admin = new Admin($item["id"],$item["username"],$item["password"],$item["text"]);
        }
        return $admin;
    }
}