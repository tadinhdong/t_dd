<?php

//class cha tạo mẫu query
class BaseQuery {
    // getAll lấy data
    static function getData($sql){
        $conn = DB::connect();
        $result = $conn->query($sql);
        $conn->close();
        if ($result->num_rows > 0) {
            return $result;
        } else{
            return $result = null;
        }
    }


    //delete and update return status
    //nếu quá trình hoàn thành thì trả về 0 ,sai thì trả về 1
    static function DE_UP($sql){
        $conn = DB::connect();
        if ($conn->query($sql) === TRUE) {

            return 0;
        } else {
            return 1;
        }
        $conn->close();
    }

}