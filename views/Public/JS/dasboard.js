$(document).ready(function () {
    $("#myInput").on("keyup", function (event) {
        event.preventDefault();
        /* Act on the event */
        var key = $(this).val().toLowerCase();
        $("#list-items tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(key) > -1);
        });
    });

    $("#option").change(function(){
        $param = {
            type:'GET',
            url:'index.php?controller=das&&action=index',
            data:{
                row_in_page:$(this).val(),
            },
            dataType:'html',
            callback:function(result){
                $('#load_view').html(result);
            }
        }
        ajax_adapter($param);
    })
});


function pagination_menu(page){
    $param = {
        type:'GET',
        url:'index.php?controller=das&&action=index',
        data:{
            page:page,
            row_in_page:$("#option").val(),
        },
        dataType:'html',
        callback:function(result){
            $('#load_view').html(result);
        }
    }
    ajax_adapter($param);
}

function delete_item(id_item){
    $param = {
        type:'POST',
        url:'index.php?controller=das&&action=delete',
        data:{
            id_item:id_item,
        },
        dataType:'html',
        callback:function(result){
            if(result=='2'){
                console.log("có lỗi xảy ra khi truyền dữ liệu vào hàm")
            }else if(result=='1'){
                $('.w3-panel').addClass("w3-red").css('display','block').html("<h3>Lỗi!</h3><p>Không thể xóa</p>").fadeOut(3000)
            }else{
                $('#load_view').html(result);
                $('.w3-panel').addClass("w3-green").css('display','block').html("<h3>Thành công!</h3><p>Xóa dữ liệu thành công</p>").fadeOut(4000)
                // setTimeout($('.w3-panel').css('display','none'), 5000);
            }

        }
    }
    ajax_adapter($param);
}