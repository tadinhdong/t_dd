$(document).ready(function(){
    $("#submit").click(function(e){
        e.preventDefault();
        let form_data = new FormData();
        let img = $("#myImage")[0].files;

        // Check image selected or not
        if(img.length > 0){
            form_data.append('my_image', img[0]);
            $.ajax({
                url: 'index.php?controller=admin&action=upload',
                type: 'post',
                data: form_data,
                contentType: false,
                processData: false,
                success: function(res){
                    const data = JSON.parse(res);
                    if (data.error != 1) {
                        let path = "http://dinhdong.net/t_dd/views/Public/uploads/"+data.src;
                        $("#preImg").attr("src", path);
                        $("#preImg").fadeOut(1).fadeIn(1000);
                        $("#myImage").val('');
                    }else {
                        $("#errorMs").text(data.em);
                    }
                }
            });

        }else {
            $("#errorMs").text("Cần chọn một ảnh.");
        }
    });
});