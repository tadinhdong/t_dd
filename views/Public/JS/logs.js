
function add_device() {
    const d = new Date();
    const name = $("#name").val();
    const ip = $("#ip").val();
    let device = {
        devices: name,
        mac_address: genMAC(),
        ip: ip,
        createdDate: d.getFullYear() + "-" + d.getMonth() + "-" + d.getDay(),
        powerConsumption: Math.floor(Math.random() * 300 + 1),
    };
    $param = {
        type:'GET',
        url:'index.php?controller=logs&&action=addItem',
        data:{
            device:device,
        },
        dataType:'html',
        callback:function(result){
            if(result=='2'){
                $('.w3-panel').addClass("w3-yellow").css('display','block').html("<h3>Cảnh báo k thêm được!</h3><p>Bị tùng tên thiết bị!</p>").fadeOut(3000)
            }else if(result=='1'){
                // w3-red
                $('.w3-panel').addClass("w3-red").css('display','block').html("<h3>Có lỗi xảy ra!</h3><p>Không thêm đc thiết bị!</p>").fadeOut(3000)
            }else{
                $('#load_view').html(result);
                $('.w3-panel').addClass("w3-green").css('display','block').html("<h3>Thêm dữ liệu thành công</h3>").fadeOut(3000)
            }
            $("#name").val(null);
            $("#ip").val(null);
        }
    }
    ajax_adapter($param);
}



function delete_device(id_item){
    $param = {
        type:'POST',
        url:'index.php?controller=logs&action=delete',
        data:{
            id_item:id_item,
        },
        dataType:'html',
        callback:function(result){
            if(result=='2'){
                console.log("có lỗi xảy ra khi truyền dữ liệu vào hàm")
            }else if(result=='1'){
                $('.w3-panel').addClass("w3-red").css('display','block').html("<h3>Lỗi!</h3><p>Không thể xóa</p>").fadeOut(3000)
            }else{
                $('#load_view').html(result);
                $('.w3-panel').addClass("w3-green").css('display','block').html("<h3>Thành công!</h3><p>Xóa dữ liệu thành công</p>").fadeOut(3000)
                // setTimeout($('.w3-panel').css('display','none'), 5000);
            }

        }
    }
    ajax_adapter($param);
}
// random màu
function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

//random a MAC address
function genMAC(){
    var hexDigits = "0123456789ABCDEF";
    var macAddress = "";
    for (var i = 0; i < 6; i++) {
        macAddress+=hexDigits.charAt(Math.round(Math.random() * 15));
        macAddress+=hexDigits.charAt(Math.round(Math.random() * 15));
        if (i != 5) macAddress += ":";
    }

    return macAddress;
}