
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
    <link rel="stylesheet" type="text/css" href="views/Public/Css/login.css" />
    <title>Login</title>
</head>
<body>
<div class="login-box">
    <h1>SOIOT SYSTEM</h1>
    <form>
        <input
            type="text"
            id="username"
            placeholder="Username"
            value="<?php if($check){ echo $admin->getUsername();}?>"
        />
        <input
            type="password"
            id="password"
            placeholder="Password"
            value="<?php if($check){ echo $admin->getPassword();}?>"
        />
        <div class="notification">Sai mật khẩu hoặc tên người dùng!</div>
        <label class="toggle" for="toggle">
            <p id="save">Save Password</p>
            <input type="checkbox" id="toggle"  <?php if($check){ echo 'checked';}?>>
            <div class="slider"></div>
        </label>
        <div>
            <input type="submit" id="form_submit" value="Login" />
            <span><a href="#">or create new account</a></span>
        </div>
    </form>
</div>
<script src="views/Public/JS/login.js"></script>
</body>
</html>
