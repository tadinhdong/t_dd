
<div class="wrapper result-content">
    <div class="row">
        <div class="col l-12 m-12 c-12">
            <div class="card">
                <div class="card-content">
                    <table>
                        <thead>
                        <tr>
                            <td>Device</td>
                            <td>MAC Address</td>
                            <td>IP</td>
                            <td>Created Date</td>
                            <td style = "text-align: center">Power Consumption(Kw/h)</td>
                            <td style = "text-align: center">Remove</td>
                        </tr>
                        </thead>
                        <tbody id="list-items">
                        <?php
                        foreach ($datas as $data) {
                            echo'<tr>
                                    <td>'.$data->getNameDevice().'</td>
                                    <td>'.$data->getMacAddress().'</td>
                                    <td>'.$data->getIpDevice().'</td>
                                    <td>'.$data->getCreateDate().'</td> 
                                    <td style = "text-align: center">'.$data->getPowerDevice().'</td>
                                    <td style = "text-align: center">
                                    <span class="remove_item" onclick="delete_device('.$data->getIdDevice().')">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash-fill" viewBox="0 0 16 16">
                                                <path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z"/>
                                            </svg>
                                        </span>
                                    </td>
                                    <tr>';
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- tv -->
    <div class="row">
        <div class="col l-6 m-12 c-12" style="padding-right: 5px">
            <div class="card">
                <div class="card-content">
                    <canvas
                            id="doughnut-chart"
                            style="width: 800px; height: 705px"
                    ></canvas>
                </div>
            </div>
        </div>
        <!-- add product -->
        <div class="col l-6 m-12 c-12" style="padding-left: 5px">
            <div class="card">
                <div class="card-content">
                    <div class="login-box">
                        <form>
                            <input type="text" name="name" id="name" placeholder="name" />
                            <input type="text" name="ip" id="ip" placeholder="IP" />
                            <input
                                    type="button"
                                    id="add-items"
                                    value="ADD DEVICE"
                                    onclick="add_device()"
                            />
                            <div id="password_error"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="views/Public/JS/logs.js"></script>
<script>
    load_chart();
    function load_chart() {
        var title = [];
        var param = [];
        var color = [];
        <?php
            for($i=0;$i < count($titles);$i++) {
        ?>
            title.push('<?php echo $titles[$i]?>');
            param.push(<?php echo $indexs[$i]?>);
            color.push(getRandomColor());
            // color.push('#'+randomColor)
        <?php
            }
        ?>
        console.log(title);
        console.log(param);
        console.log("hihi")
        new Chart(document.getElementById("doughnut-chart"), {
            type: "doughnut",
            data: {
                labels: title,
                datasets: [
                    {
                        label: "Population (millions)",
                        backgroundColor: color,
                        data: param,
                    },
                ],
            },
            options: {
                title: {
                    display: true,
                    text: "Power Consumption(Kw/h)",
                },
            },
        });
    }
</script>