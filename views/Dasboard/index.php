<div class="wrapper result-content">
    <div class="row">
        <div class="col l-12 m-12 c-12">
            <div class="topnav">
                <h1>Action Logs</h1>
                <input
                        type="text"
                        id="myInput"
                        placeholder="Search.."
                        style="margin-top: 23px"
                />

                <select name="option" id="option">
                    <option value="5" <?php if($row_in_page==5){echo "selected";}?> >5</option>
                    <option value="10" <?php if($row_in_page==10){echo "selected";}?>>10</option>
                    <option value="12" <?php if($row_in_page==12){echo "selected";}?>>12</option>
                    <option value="15" <?php if($row_in_page==15){echo "selected";}?>>15</option>
                </select>
            </div>
        </div>
        <div class="col l-12 m-12 c-12">
            <div class="card">
                <div class="card-content">
                    <table id="index">
                        <thead>
                        <tr>
                            <td>Device #</td>
                            <td>Name</td>
                            <td>Action</td>
                            <td>Created Date</td>
                            <td style = "text-align: center">Remove</td>
                        </tr>
                        </thead>
                        <tbody id="list-items">
                        <?php
                        foreach ($datas as $data) {
                            echo'<tr>
                                    <td>'.$data->getIdLog().'</td>
                                    <td>'.$data->getDevice().'</td>
                                    <td>'.$data->getAction().'</td>
                                    <td>'.$data->getDate().'</td>
                                    <td style = "text-align: center">
                                        <span onclick="delete_item('.$data->getIdLog().')" class="remove_item">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash-fill" viewBox="0 0 16 16">
                                                <path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z"/>
                                            </svg>
                                        </span>
                                    </td>
                                    <tr>';
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="out_page">
            <div id="page_navigation">
                <?php if($page>2){ ?>
                    <a class="page_link" onclick="pagination_menu(1)">Đầu</a>
                <?php } ?>

                <?php if($page>1){ ?>
                    <a class="previous_link" onclick="pagination_menu(<?php echo $page-1;?>)">«</a>
                <?php } ?>

                <?php for($i=1;$i<=$number_page;$i++) {?>
                    <?php  if($i> $page-2 && $i< $page+2){ ?>
                    <a class="page_link" onclick="pagination_menu(<?php echo $i;?>)"
                        <?php if($page == $i){
                            echo "style='background:tomato'";
                        }
                        ?>
                    ><?php echo"$i";?></a>
                    <?php } ?>
                <?php } ?>

                <?php if($page<$number_page-1){ ?>
                    <a class="next_link" onclick="pagination_menu(<?php echo $page+1;?>)">»</a>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<script src="views/Public/JS/dasboard.js"></script>


