<?php
session_start();
require_once('./connect.php');
//kiểm tra controller
if (isset($_GET['controller'])) {
$controller = $_GET['controller'];
//kiểm tra action
if (isset($_GET['action'])) {
    //nếu k có action
    $action = $_GET['action'];
} else {
    $action = 'index';
}
} else {
    //nếu k có controller thì gán mặc định là :
    $controller = 'logs';
    $action = 'index';
}
require_once('./routes.php');