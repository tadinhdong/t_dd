<?php
//khai báo controller và action (đây coi như là router)
// quy tắc tencontroller => [các ation]
$controllers = array(
    'das' => ['index','error','delete'],
    'logs' => ['index','error','addItem','delete'],
    'login' =>['index','login','logout'],
    'admin' =>['index','upload'],
);
//kiểm tra có controller và action thảo mãn router k
if (!array_key_exists($controller, $controllers) || !in_array($action, $controllers[$controller])) {
    $controller = 'logs';
    $action = 'index';
}
include_once('controllers/' . $controller . '_controller.php');
//echo 'controllers/' . $controller . '_controller.php';

$klass = str_replace('_', '', ucwords($controller, '_')) . 'Controller';
//echo $klass;
$controller = new $klass;
$controller->$action();
